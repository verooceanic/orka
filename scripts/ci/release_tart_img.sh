#!/usr/bin/env zsh
set -euxo pipefail

if [[ ! -v BASE_IMAGE_NAME ]] || [[ ! -v MACOS_VERSION ]] || [[ ! -v XCODE_VERSION ]]; then
  echo "Please set ROLE, BASE_IMAGE_NAME, MACOS_VERSION, XCODE_VERSION"
fi

REPOSITORY_NAME=$(echo "$BASE_IMAGE_NAME" | cut -d: -f1)
SOURCE_TAG=$(echo "$BASE_IMAGE_NAME" | cut -d: -f2)

TIMESTAMP=$(date -jf "%Y-%m-%dT%H:%M:%SZ" "$CI_PIPELINE_CREATED_AT" "+%Y%m%d%H%M")
DEST_TAG="macos-$MACOS_VERSION-xcode-$XCODE_VERSION-$TIMESTAMP"

MANIFEST=$(aws ecr batch-get-image --region eu-west-1 --repository-name "$REPOSITORY_NAME" --image-ids "imageTag=$SOURCE_TAG" --query 'images[0].imageManifest' --output text)
aws ecr put-image --no-paginate --region eu-west-1 --repository-name "$REPOSITORY_NAME" --image-tag "$DEST_TAG" --image-manifest "$MANIFEST"
