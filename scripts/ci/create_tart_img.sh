#!/usr/bin/env zsh
set -euxo pipefail

if [[ ! -v ROLE ]] || [[ ! -v BASE_IMAGE_NAME ]] || [[ ! -v TOOLCHAIN_VERSION ]]; then
  echo "Please set ROLE, BASE_IMAGE_NAME, TOOLCHAIN_VERSION"
fi

VM_NAME="p-$CI_PIPELINE_ID-$CI_JOB_NAME_SLUG"
if [[ -z ${CI_COMMIT_BRANCH:-} || "$CI_COMMIT_BRANCH" != "$CI_DEFAULT_BRANCH" ]]; then
  # for MRs, prefix the image name with the MR ID
  VM_NAME="mr-$CI_MERGE_REQUEST_IID-$VM_NAME"
fi

TART_VERSION=0.32.1
ECR_ENDPOINT="915502504722.dkr.ecr.eu-west-1.amazonaws.com"
DEST_IMAGE="macos/$ROLE:$VM_NAME"

# we're skipping the "release" role on ARM as we don't need to set a secret password
# so let's push directly to the release repo
if [ $ROLE = "brew" ]; then
  DEST_IMAGE="macos/release:$VM_NAME"
fi

# install/upgrade tart if it's not the version we want
if [[ $(tart --version 2> /dev/null) != $TART_VERSION ]]; then
  curl -OL https://github.com/cirruslabs/tart/releases/download/${TART_VERSION}/tart.tar.gz
  tar -O -xzvf tart.tar.gz tart > tartcli
  rm -f tart.tar.gz
  chmod +x tartcli
  sudo mv tartcli /usr/local/bin/tart
  echo "installed tart $TART_VERSION"
fi

# Pull the source image from ECR
tart pull "$ECR_ENDPOINT/$BASE_IMAGE_NAME"

_delete_vm() {
  # make sure we don't leave VMs behind
  tart delete "$VM_NAME" || true
}
trap _delete_vm EXIT

packer init tart/
packer build -color=false \
  -var source_image="$ECR_ENDPOINT/$BASE_IMAGE_NAME" \
  -var vm_name="$VM_NAME" \
  -var role="$ROLE" \
  -var toolchain_version="$TOOLCHAIN_VERSION" \
  -var xcode_version="${XCODE_VERSION:-}" \
  tart/macos.pkr.hcl

# Clone the built image (so the specs don't modify it) and run it in the background
tart clone "$VM_NAME" "spec-$VM_NAME"
tart run --no-graphics "spec-$VM_NAME" &
TART_IP=$(tart ip --wait 120 "spec-$VM_NAME")

_delete_spec_vm() {
  # make sure we don't leave VMs behind
  kill %1 || true
  tart delete "spec-$VM_NAME" || true
}
trap '_delete_spec_vm; _delete_vm' EXIT

# Run specs
set +e
LOGIN_PASSWORD=gitlab TARGET_HOST="$TART_IP" TARGET_PORT=22 ROLES_TO_TEST="${ADDITONAL_SPECS:-},$ROLE" TOOLCHAIN_VERSION=$TOOLCHAIN_VERSION bundle exec rake spec
retval=$? # we capture the failure here because we still want to push the image for troubleshooting if the specs failed
set -e
if [ $retval -ne 0 ]; then
  DEST_IMAGE="$DEST_IMAGE-failed" # don't mistake this for a working image
fi

trap _delete_vm EXIT
_delete_spec_vm

tart push "$VM_NAME" "$ECR_ENDPOINT/$DEST_IMAGE"

# save image name for next stage
echo "BASE_IMAGE_NAME=$DEST_IMAGE" > .base-image.env

# exit using specs exit code
exit $retval
