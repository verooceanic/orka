#!/usr/bin/env zsh
set -euo pipefail

INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
ASG_NAME=$(curl -s http://169.254.169.254/latest/meta-data/tags/instance/aws:autoscaling:groupName)
REGION=$(curl -s http://169.254.169.254/latest/meta-data/placement/region)

# Maximum number of VMs is 2, so set concurrent to 2
sed -i ' ' "s|^concurrent.*|concurrent = 2|" /Users/ec2-user/.gitlab-runner/config.toml

# Get the registration token from SSM
REGISTRATION_TOKEN=$(aws ssm get-parameter --region "$REGION" --name orka.runner_registration_token --with-decryption --output text --query Parameter.Value)

# Register the runner
# Note: maximum timeout comes from maximum allowed time by AWS for termination lifecycle hook to complete in the instance's ASG
su -l ec2-user -c "gitlab-runner register --non-interactive --registration-token \"$REGISTRATION_TOKEN\" --description \"macos shell runner $INSTANCE_ID\" --maximum-timeout 7200 --url \"https://gitlab.com/\" --executor shell --tag-list \"gitlab-org-aws-macos,$INSTANCE_ID\""

# Lifecycle completion
aws autoscaling complete-lifecycle-action --lifecycle-action-result CONTINUE --instance-id "$INSTANCE_ID" --lifecycle-hook-name "startup" --auto-scaling-group-name "$ASG_NAME" --region "$REGION"
