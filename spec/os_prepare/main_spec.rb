# frozen_string_literal: true

require 'spec_helper'

context 'roles/os_prepare' do
  # check that we set the system timezone to GMT / UTC
  describe command('sudo systemsetup -gettimezone') do
    it { should be_a_successful_cmd }
    its(:stdout) { should match(/Time Zone: GMT/) }
  end

  # ensure Xcode command line tools are installed
  describe file('/Library/Developer/CommandLineTools/usr/bin/git') do
    it { should be_file }

    it 'is able to clone repositories' do
      with_tmpdir do |tmpdir|
        cmd = command("#{subject.name} clone https://github.com/mizzy/serverspec.git #{tmpdir}")
        expect(cmd).to be_a_successful_cmd
      end
    end
  end

  # If auto login is working, we should be in a graphical session, so Finder should be running
  # Skipping for now as is flaky
  describe process('Finder'), skip: 'Detecting graphical session is flaky' do
    it { should be_running }
  end
end
